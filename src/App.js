import React, { Component } from 'react';
import './App.css';

import Person from './Person/person.js';

class App extends Component {
  state = {
    person : [
      {name: 'Max', age: 27},
      {name: 'Ken', age: 29},
      {name: 'Sue', age: 30},
    ],

    showPerson: false,
  }

  switchName = (newName) => {
    this.setState({
      person: [
        {name: newName, age: 2777 },
        {name: 'Ken', age: 29 },
        {name: 'Sue', age: 30 },
      ]
    })
  }

  nameChange = (event) => {
    this.setState({
      person: [
        {name: 'ABC', age: 2777 },
        {name: event.target.value, age: 29 },
        {name: 'Sue', age: 30 },
      ]
    })
  }

  togglePerson = () => {
    const show = this.state.showPerson;
    this.setState({showPerson: !show});
  }

  render() {
    let person = null;

    if(this.state.showPerson) {
      person = (
        <div>
        <Person
          name={this.state.person[0].name}
          age={this.state.person[0].age} />
        <Person
          name={this.state.person[1].name}
          age={this.state.person[1].age}
          click={this.switchName.bind(this, 'CBA')}
          change={this.nameChange}>
          I'm handsome</Person>
        <Person
          name={this.state.person[2].name}
          age={this.state.person[2].age}/>
      </div>
      );
    }

    return (
      <div className="App">
        <button onClick={this.togglePerson}>Click</button>
        {person}
      </div>
    );

  }
}

export default App;
