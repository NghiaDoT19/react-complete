import React from 'react';

const person = (props) => {
    return (
        <div>
            <p onClick={props.click}>Hello, I'm {props.name} and I'm {props.age} year old!!!</p>
            <p>{props.children}</p>
            <input type="text" onChange={props.change} />
        </div>
    )
}

export default person;